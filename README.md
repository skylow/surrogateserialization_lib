# SurrogateSerialization lib

A librairy that allow you to serialize unserializable object using surrogate

version: 1.0.0

## Usage

- Example (Your unsierializable object): 
```javascript
/**
 * Class Example
 * @author Skylow
 *
 */
public class Example {
	private ArrayList<String> exampleList;
	
	public Example() {
		this.exampleList = new ArrayList<String>();
	}
	
	/**
	 * 
	 * @return the number of element in exampleList
	 */
	public int size() {
		return this.exampleList.size();
	}
	
	/**
	 * 
	 * @param i
	 * @return the object at the index specified
	 */
	public String getStringExample(int i) {
		return this.exampleList.get(i);
	}
	
	/**
	 * 
	 * @param s
	 * @return the index of s
	 */
	public int indexOf(String s) {
		return this.exampleList.indexOf(s);
	}
	
	/**
	 * Add s to the list if the list doesn't contains the object s
	 * @param s
	 */
	public void add(String s) {
		if(!this.exampleList.contains(s))
			this.exampleList.add(s);
	}
	
	@Override
	public String toString() {
		return "[Example " + this.exampleList.toString() + "]";
	}
}
```

- The surrogate (of Example class): 
```javascript
/**
 * Surrogate Class
 * @author Skylow
 *
 */
public class ExampleSurrogate  implements Serializable, Surrogate<Example> {
	private static final long serialVersionUID = 1L;
	public final String SEPARATOR = "/";
	private String foo;
	
	public ExampleSurrogate(Example e) {
		this.foo = exampleToString(e);
	}
	
	@Override
	public Object resolve() {
		return stringToExample();
	}
	
	/**
	 * Convert an Example to a string like this: elt1/elt2/.../eltn
	 * @param e
	 * @return the generate string
	 */
	public String exampleToString(Example e) {
		String toString = "";
		for(int i=0;i<e.size();i++)
			toString += (e.getStringExample(i) + this.SEPARATOR);
		
		return toString.substring(0, toString.length() - 1);
	}
	
	/**
	 * Convert a string like this (elt1/elt2/.../eltn) to an Example
	 * @return the generate Example
	 */
	public Example stringToExample() {
		String[] values = this.foo.split(this.SEPARATOR);
		Example e = new Example();
		
		for(String s : values)
			e.add(s);
		
		return e;
	}
	
}
```

- The surrogate factory : 
```javascript
/**
 * The surrogate Example Factory
 * @author Skylow
 *
 */
public class SurrogateExampleFactory implements SurrogateFactory<Example> {

	@Override
	public Class<Example> getTargetClass() {
		return Example.class;
	}

	@Override
	public Object newSurrogate(Example target) {
		return new ExampleSurrogate(target);
	}
}
```

- The serialization methods : 
```javascript
/**
 * Class of Serialization
 * @author Skylow
 *
 */
public class SurrogateSerializationMethodsExample {
	/**
	 * Serialize the object o
	 * @param o
	 * @return the serialization of the object o
	 * @throws IOException
	 */
	public static byte[] serialize(Object o) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		SurrogateFactoryRegistry registry = initRegistry();
        SurrogateObjectOutputStream os = new SurrogateObjectOutputStream(baos, registry);
        
        os.writeObject(o);
        os.flush();
        os.close();
        
        return baos.toByteArray();
	}
	
	/**
	 * Deserialize the serialized object bytes
	 * @param bytes
	 * @return the object associate to the serialized object
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        
        SurrogateFactoryRegistry registry = initRegistry();
        SurrogateObjectInputStream is = new SurrogateObjectInputStream(bais, registry);
        
        Object o = is.readObject();
        bais.close();
        is.close();
        
        return o;
    }
	
	/**
	 * 
	 * @return a registry with all the factory registered
	 */
	private static SurrogateFactoryRegistry initRegistry() {
		SurrogateFactoryRegistry registry = new SurrogateFactoryRegistry();
        registry.registerSurrogateFactory(new SurrogateExampleFactory());
        // Put here all the Surrogate Factory you want to use ...
        return registry;
	}
}
```

- The Main : 
```javascript
/**
 * Test class using Example
 * @author Skylow
 *
 */
public class Test {

	public static void main(String[] args) {
		byte[] serializedExample;		// The serialize Example
		ExampleSurrogate surrogate;		// The surrogate extract from serializedObject
		Example e;						// The example to serialized
		
		// Creation of the Example
		e = new Example();
		e.add("First String"); e.add("Hooo an other !");

		System.out.println("Example to serialize: " + e);
		
		//Initialization of the serialized object
		serializedExample = null;
		
		//Serialization
		try {
			serializedExample = SurrogateSerializationMethodsExample.serialize(e);
			System.out.println("Serialization: " + serializedExample);
		} catch (IOException e1) {
			System.out.println("Failed to serialize !");
			e1.printStackTrace();
		}
		
		//Unserialization
		try {
			surrogate = (ExampleSurrogate) SurrogateSerializationMethodsExample.deserialize(serializedExample);
			System.out.println("Unserialization: " + ((Example) surrogate.resolve()));
		} catch (ClassNotFoundException | IOException e1) {
			System.out.println("Failed to unserialize !");
			e1.printStackTrace();
		}
	}

}
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
No license available now