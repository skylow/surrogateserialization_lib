package modele.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

import modele.interfaces.SurrogateFactory;
import modele.registry.SurrogateFactoryRegistry;

/**
 * Custom ObjectInputStream that allow resolveObject (Surrogate -> obj)
 * @author Skylow
 *
 */
public class SurrogateObjectInputStream extends ObjectInputStream {
	private final SurrogateFactoryRegistry registry;

	public SurrogateObjectInputStream(InputStream in, SurrogateFactoryRegistry registry) throws IOException {
        super(in);
        this.registry = registry;
       	enableResolveObject(true);
    }

	protected SurrogateObjectInputStream(SurrogateFactoryRegistry registry) throws IOException, SecurityException {
        super();
        this.registry = registry;
        enableResolveObject(true);
    }
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Object resolveObject(Object obj) throws IOException {
		SurrogateFactory surrogateFactory = registry.getSurrogateFactory(obj.getClass());
		
		if(surrogateFactory != null) {
			try {
				return surrogateFactory.getTargetClass().newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
			return super.resolveObject(obj);
			//return surrogateFactory.newSurrogate(obj);
		} else
			return super.resolveObject(obj);
	}
}
