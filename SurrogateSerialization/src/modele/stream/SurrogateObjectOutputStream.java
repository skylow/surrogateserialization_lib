package modele.stream;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import modele.interfaces.SurrogateFactory;
import modele.registry.SurrogateFactoryRegistry;

/**
 * Custom ObjectOutputStream that allow replaceObject (obj -> Surrogate)
 * @author Skylow
 *
 */
public class SurrogateObjectOutputStream extends ObjectOutputStream {

	private final SurrogateFactoryRegistry registry;
	
	public SurrogateObjectOutputStream(OutputStream out, SurrogateFactoryRegistry registry) throws IOException {
        super(out);
        this.registry = registry;
        enableReplaceObject(true);
    }
 
    protected SurrogateObjectOutputStream(SurrogateFactoryRegistry registry) throws IOException, SecurityException {
        super();
        this.registry = registry;
        enableReplaceObject(true);
    }
 
    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected Object replaceObject(Object obj) throws IOException {
        SurrogateFactory surrogateFactory = registry.getSurrogateFactory(obj.getClass());
        if (surrogateFactory != null) {
            return surrogateFactory.newSurrogate(obj);
        } else return super.replaceObject(obj);
    }

}
