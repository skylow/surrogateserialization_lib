package modele.interfaces;

/**
 * Surrogate Factory interface
 * @author Skylow
 *
 * @param <T>
 */
public interface SurrogateFactory<T> {
	/**
	 * 
	 * @return the class of the object associate to the Surrogate
	 */
	Class<T> getTargetClass();
	/**
	 * 
	 * @param target
	 * @return a Surrogate associate to the object
	 */
    Object newSurrogate(T target);
}
