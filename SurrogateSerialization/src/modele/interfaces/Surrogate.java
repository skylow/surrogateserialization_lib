package modele.interfaces;

/**
 * Surrogate Interface
 * @author Skylow
 * T = The object type to whom associate the Surrogate
 * @param <T>
 */
public interface Surrogate<T> {
	/**
	 * 
	 * @return the object associate to the Surrogate
	 */
	public Object resolve();
}
