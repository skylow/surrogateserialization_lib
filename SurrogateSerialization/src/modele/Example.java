package modele;

import java.util.ArrayList;

/**
 * Class Example
 * @author Skylow
 *
 */
public class Example {
	private ArrayList<String> exampleList;
	
	public Example() {
		this.exampleList = new ArrayList<String>();
	}
	
	/**
	 * 
	 * @return the number of element in exampleList
	 */
	public int size() {
		return this.exampleList.size();
	}
	
	/**
	 * 
	 * @param i
	 * @return the object at the index specified
	 */
	public String getStringExample(int i) {
		return this.exampleList.get(i);
	}
	
	/**
	 * 
	 * @param s
	 * @return the index of s
	 */
	public int indexOf(String s) {
		return this.exampleList.indexOf(s);
	}
	
	/**
	 * Add s to the list if the list doesn't contains the object s
	 * @param s
	 */
	public void add(String s) {
		if(!this.exampleList.contains(s))
			this.exampleList.add(s);
	}
	
	@Override
	public String toString() {
		return "[Example " + this.exampleList.toString() + "]";
	}
}
