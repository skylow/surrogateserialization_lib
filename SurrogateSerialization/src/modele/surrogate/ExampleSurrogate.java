package modele.surrogate;

import java.io.Serializable;

import modele.Example;
import modele.interfaces.Surrogate;

/**
 * Surrogate Class
 * @author Skylow
 *
 */
public class ExampleSurrogate  implements Serializable, Surrogate<Example> {
	private static final long serialVersionUID = 1L;
	public final String SEPARATOR = "/";
	private String foo;
	
	public ExampleSurrogate(Example e) {
		this.foo = exampleToString(e);
	}
	
	@Override
	public Object resolve() {
		return stringToExample();
	}
	
	/**
	 * Convert an Example to a string like this: elt1/elt2/.../eltn
	 * @param e
	 * @return the generate string
	 */
	public String exampleToString(Example e) {
		String toString = "";
		for(int i=0;i<e.size();i++)
			toString += (e.getStringExample(i) + this.SEPARATOR);
		
		return toString.substring(0, toString.length() - 1);
	}
	
	/**
	 * Convert a string like this (elt1/elt2/.../eltn) to an Example
	 * @return the generate Example
	 */
	public Example stringToExample() {
		String[] values = this.foo.split(this.SEPARATOR);
		Example e = new Example();
		
		for(String s : values)
			e.add(s);
		
		return e;
	}
	
}
