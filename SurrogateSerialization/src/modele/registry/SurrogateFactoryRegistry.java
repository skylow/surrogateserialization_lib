package modele.registry;

import java.util.HashMap;
import java.util.Map;

import modele.interfaces.SurrogateFactory;

/**
 * Factory registry
 * @author Skylow
 *
 */
public class SurrogateFactoryRegistry {
	private final Map<Class<?>, SurrogateFactory<?>> surrogates = new HashMap<Class<?>, SurrogateFactory<?>>();
	 
	/**
	 * Register a factory to the registry
	 * @param Surrogate
	 */
    public void registerSurrogateFactory(SurrogateFactory<?> surrogate) {
        surrogates.put(surrogate.getTargetClass(), surrogate);
    }
 
    /**
     * 
     * @param targetClass
     * @return the factory associate to a Surrogate
     */
    @SuppressWarnings("unchecked")
    public <T> SurrogateFactory<T> getSurrogateFactory(Class<T> targetClass) {
        return (SurrogateFactory<T>) surrogates.get(targetClass);
    }
}
