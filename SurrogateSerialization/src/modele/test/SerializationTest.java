package modele.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import modele.Example;
import modele.example.SurrogateSerializationMethodsExample;
import modele.surrogate.ExampleSurrogate;

class SerializationTest {

	@Test
	void test() {
		// Test the serialize / unserialize method. Return false if exception if throw, true else
		try {
			runSerialization();
		} catch (ClassNotFoundException | IOException e1) {
			e1.printStackTrace();
			assertFalse(true);
		}
		
		assertTrue(true);
	}
	
	private void runSerialization() throws IOException, ClassNotFoundException {
		byte[] serializedExample;		// The serialize Example
		ExampleSurrogate surrogate;		// The surrogate extract from serializedObject
		Example e;						// The example to serialized
		
		// Creation of the Example
		e = new Example();
		e.add("First String"); e.add("Hooo an other !");

		System.out.println("Example to serialize: " + e);
		
		//Initialization of the serialized object
		serializedExample = null;
		
		//Serialization
		serializedExample = SurrogateSerializationMethodsExample.serialize(e);
		System.out.println("Serialization: " + serializedExample);
		
		//Unserialization
		surrogate = (ExampleSurrogate) SurrogateSerializationMethodsExample.deserialize(serializedExample);
		System.out.println("Unserialization: " + ((Example) surrogate.resolve()));
	}

}
