package modele.example;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import modele.registry.SurrogateFactoryRegistry;
import modele.stream.SurrogateObjectInputStream;
import modele.stream.SurrogateObjectOutputStream;

/**
 * Class of Serialization
 * @author Skylow
 *
 */
public class SurrogateSerializationMethodsExample {
	/**
	 * Serialize the object o
	 * @param o
	 * @return the serialization of the object o
	 * @throws IOException
	 */
	public static byte[] serialize(Object o) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		SurrogateFactoryRegistry registry = initRegistry();
        SurrogateObjectOutputStream os = new SurrogateObjectOutputStream(baos, registry);
        
        os.writeObject(o);
        os.flush();
        os.close();
        
        return baos.toByteArray();
	}
	
	/**
	 * Deserialize the serialized object bytes
	 * @param bytes
	 * @return the object associate to the serialized object
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        
        SurrogateFactoryRegistry registry = initRegistry();
        SurrogateObjectInputStream is = new SurrogateObjectInputStream(bais, registry);
        
        Object o = is.readObject();
        bais.close();
        is.close();
        
        return o;
    }
	
	/**
	 * 
	 * @return a registry with all the factory registered
	 */
	private static SurrogateFactoryRegistry initRegistry() {
		SurrogateFactoryRegistry registry = new SurrogateFactoryRegistry();
        registry.registerSurrogateFactory(new SurrogateExampleFactory());
        // Put here all the Surrogate Factory you want to use ...
        return registry;
	}
}
