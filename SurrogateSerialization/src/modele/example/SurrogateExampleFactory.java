package modele.example;
import modele.Example;
import modele.interfaces.SurrogateFactory;
import modele.surrogate.ExampleSurrogate;

/**
 * The surrogate Example Factory
 * @author Skylow
 *
 */
public class SurrogateExampleFactory implements SurrogateFactory<Example> {

	@Override
	public Class<Example> getTargetClass() {
		return Example.class;
	}

	@Override
	public Object newSurrogate(Example target) {
		return new ExampleSurrogate(target);
	}
}
